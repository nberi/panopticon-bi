(function() {
var toc =  [{"type":"item","name":"Creating Delta Drill charts","url":"Creating_Delta_Drill_charts.htm"},{"type":"item","name":"Creating Slope charts","url":"Creating_Slope_charts.htm"},{"type":"item","name":"Creating Radar charts","url":"Creating_Radar_Charts.htm"},{"type":"item","name":"Creating Word Cloud charts","url":"Creating_Word_Cloud_charts.htm"},{"type":"item","name":"Creating Heat Map charts","url":"Creating_Heat_Map_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();