(function() {
var toc =  [{"type":"item","name":"Controlling Report-level Access","url":"Controlling_Report-level_Access.htm"},{"type":"item","name":"Exporting Reports","url":"Exporting_Reports.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();