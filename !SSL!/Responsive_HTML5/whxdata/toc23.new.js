(function() {
var toc =  [{"type":"item","name":"Creating Boxplot charts","url":"Creating_Boxplot_charts.htm"},{"type":"item","name":"Creating Point charts","url":"Creating_Scatter_charts.htm"},{"type":"item","name":"Creating Bubble charts","url":"Creating_Bubble_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();