(function() {
var toc =  [{"type":"item","name":"Creating Line charts","url":"Creating_Line_charts.htm"},{"type":"item","name":"Creating Stacked Line charts","url":"Creating_Stacked_Line_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();