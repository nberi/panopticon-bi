(function() {
var toc =  [{"type":"item","name":"Creating Rich Text charts","url":"Creating_Richtext.htm"},{"type":"item","name":"Creating External URL charts","url":"Creating_External_URL_charts.htm"},{"type":"item","name":"Creating GUI Forms","url":"Creating_GUI_Forms.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();