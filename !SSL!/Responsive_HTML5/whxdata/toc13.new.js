(function() {
var toc =  [{"type":"item","name":"Creating Bar Line charts","url":"Creating_Bar_Line_charts.htm"},{"type":"item","name":"Creating Stacked Bar charts","url":"Creating_Stcaked_Bar_charts.htm"},{"type":"item","name":"Creating Percent Bar charts","url":"Creating_Percent_Bar_charts.htm"},{"type":"item","name":"Creating Histogram charts","url":"Creating_Histogram_charts.htm"},{"type":"item","name":"Creating Waterfall charts","url":"Creating_Waterfall_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();