(function() {
var toc =  [{"type":"item","name":"Creating a New Report","url":"Creating_a_new_Report.htm"},{"type":"item","name":"Copying Reports","url":"Copying_a_Report.htm"},{"type":"item","name":"Deleting a Report","url":"Deleting_Reports.htm"},{"type":"item","name":"Importing Report Templates","url":"Importing_Report_Templates.htm"},{"type":"item","name":"Embedding Links","url":"Embedding_Links.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();