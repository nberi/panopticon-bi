(function() {
var toc =  [{"type":"item","name":"Creating Map charts","url":"Creating_Map_charts.htm"},{"type":"item","name":"Creating Floorplan charts","url":"Creating_Floorplan_charts.htm"},{"type":"item","name":"Creating Custom Map charts","url":"Creating_Custom_Map_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();