(function() {
var toc =  [{"type":"item","name":"Creating Table charts","url":"Creating_Tabular_charts.htm"},{"type":"item","name":"Creating Tree Table charts","url":"Creating_Tree_Table_charts.htm"},{"type":"item","name":"Creating Rollup charts","url":"Creating_Rollup_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();