(function() {
var toc =  [{"type":"item","name":"Connecting to Dynamics365","url":"Connecting_to_Dynamics365.htm"},{"type":"item","name":"Connecting to Salesforce","url":"Connecting_to_Sales_Force.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();