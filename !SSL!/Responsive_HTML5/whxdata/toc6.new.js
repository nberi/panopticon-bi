(function() {
var toc =  [{"type":"item","name":"Connecting to Access Files","url":"Connecting_to_Access_Files.htm"},{"type":"item","name":"Connecting to CSV Files","url":"Uploading_Files.htm"},{"type":"item","name":"Connecting to Excel Files","url":"Connecting_to_Excel.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();